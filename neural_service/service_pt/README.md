# Запуск сервиса

## Запуск локально

### Настройка окружения

1. Создать и активировать venv:

    ```bash
    python3 -m venv venv
    . venv/bin/activate
    ```

1. Установка зависимостей

   ```bash
   make install
   ```

1. Перенести последнюю лучшую модель в папку [weights](weights) (переименовать модель в
   `bestmodel.pt`)
1. Запуск сервиса

    ```bash
    python3 -m uvicorn app:app --host='0.0.0.0' --port=$(APP_PORT)
    ```

    где `APP_PORT` - порт по которому будет осуществляться подключение
1. Запуск докера (build Dockerfile)

    ```bash
    make build DOCKER_IMAGE=$(DOCKER_IMAGE) DOCKER_TAG=$(DOCKER_TAG)
    ```

    `DOCKER_IMAGE` - имя докер образа, `DOCKER_TAG` - тег образа контейнера

    ```bash
    docker run \
    -d \
    -p 0.0.0.0:5000 \
    --name=$(CONTAINER_NAME) \
    ${DOCKER_IMAGE}
    ```

    `CONTAINER_NAME` - имя контейнера

## Тестирование

```bash
PYTHONPATH=. pytest .
```
