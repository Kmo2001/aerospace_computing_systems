import cv2
import numpy as np
import tensorflow as tf


def preprocess_image(image: np.ndarray, input_shape: tuple) -> np.ndarray:
    image = cv2.resize(image, input_shape[1:3])
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = image / 255.0
    image = np.array(image, dtype=np.float32)
    image = np.expand_dims(image, axis=0)

    return image


def load_model(path):
    interpreter = tf.lite.Interpreter(model_path=path)
    interpreter.allocate_tensors()

    return interpreter
